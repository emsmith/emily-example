// stdlib functionality
#include <iostream>

// ROOT functionality
#include <TFile.h>
#include <TH1D.h>
#include <TCanvas.h>

// ATLAS EDM functionality
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODJet/JetContainer.h"

#include "JetSelectionHelper/JetSelectionHelper.h"
#include "AsgTools/AnaToolHandle.h"
#include "JetCalibTools/IJetCalibrationTool.h"


int main(int argc, char** argv) {

  // initialize the xAOD EDM
  xAOD::Init();

  // open the input file
  TString inputFilePath = "mc16_13TeV.345055.PowhegPythia8EvtGen_NNPDF3_AZNLO_ZH125J_MINLO_llbb_VpT.deriv.DAOD_EXOT27.e5706_s3126_r10724_p3840/DAOD_EXOT27.17882744._000026.pool.root.1";
  if(argc >= 2) inputFilePath = argv[1];
  TString outputFilePath = "Plots.root";
  if(argc >= 3) outputFilePath = argv[2];    
  TFile *oFile = new TFile(outputFilePath, "RECREATE");
  
  xAOD::TEvent event;
  std::unique_ptr< TFile > iFile ( TFile::Open(inputFilePath, "READ") );
  if(!iFile) return 1;
  event.readFrom( iFile.get() );

  // get the number of events in the file to loop over
  Long64_t numEntries = -1; //event.getEntries();
  if (argc >= 4) numEntries = std::atoi(argv[3]);
  if (numEntries == -1) numEntries = event.getEntries();
  std::cout << "Processing " << numEntries << " events" << std::endl;
  
  //Declare and Define histograms
  TH1D *h_njets = new TH1D("h_njets", "h_njets", 20, 0, 20);
  TH1D *h_mjj = new TH1D("h_mjj", "h_mjj", 20, 0, 500);
  TH1D *h_njets_cal = new TH1D("h_njets", "h_njets", 20, 0, 20);
  TH1D *h_mjj_cal = new TH1D("h_mjj", "h_mjj", 20, 0, 500);

  //Define CalibrationTool
  asg::AnaToolHandle<IJetCalibrationTool> JetCalibrationTool_handle;
  JetCalibrationTool_handle.setTypeAndName("JetCalibrationTool/MyCalibrationTool");
  JetCalibrationTool_handle.setProperty("JetCollection","AntiKt4EMTopo");
  JetCalibrationTool_handle.setProperty("ConfigFile", "JES_MC16Recommendation_Consolidated_EMTopo_Apr2019_Rel21.config");
  JetCalibrationTool_handle.setProperty("CalibSequence", "JetArea_Residual_EtaJES_GSC_Smear");
  JetCalibrationTool_handle.setProperty("CalibArea", "00-04-82" );
  JetCalibrationTool_handle.setProperty("IsData", false); 
  JetCalibrationTool_handle.retrieve();

  // for counting events
  unsigned count = 0;

  
  // primary event loop
  for ( Long64_t i=0; i<numEntries; ++i ) {

    // Load the event
    event.getEntry( i );

    // Load xAOD::EventInfo and print the event info
    const xAOD::EventInfo * ei = nullptr;
    event.retrieve( ei, "EventInfo" );
    if(i%10000 == 0) std::cout << "Processing run # " << ei->runNumber() << ", event # " << ei->eventNumber() << std::endl;

    // retrieve the jet container from the event store
    const xAOD::JetContainer* jets = nullptr;
    event.retrieve(jets, "AntiKt4EMTopoJets");

    //Make accessible vector of jets
    std::vector<xAOD::Jet> vjets;
    std::vector<xAOD::Jet> cjets;

    //Add jet selection helper
    JetSelectionHelper jet_selector;

    // loop through all of the jets and make selections with the helper
    for(const xAOD::Jet* jet : *jets) {
      // print the kinematics of each jet in the event (Actually please don't)
      if(i%10000 == 0) std::cout << "Jet : pt=" << jet->pt() << "  eta=" << jet->eta() << "  phi=" << jet->phi() << "  m=" << jet->m() << std::endl;
      
      xAOD::Jet *calibratedjet;
      JetCalibrationTool_handle->calibratedCopy(*jet, calibratedjet);
      
      if (jet_selector.isJetGood(jet)) {
        vjets.push_back(*jet);
      }
      if (jet_selector.isJetGood(calibratedjet)) {
        cjets.push_back(*calibratedjet);
      }
      delete calibratedjet;
    }

    h_njets->Fill( vjets.size());
    h_njets_cal->Fill( cjets.size());
 
    if( vjets.size() >= 2)
      h_mjj->Fill( (vjets.at(0).p4() + vjets.at(1).p4()).M() / 1000);
    if( cjets.size() >= 2)
      h_mjj_cal->Fill( (cjets.at(0).p4() + cjets.at(1).p4()).M() / 1000);

    // counter for the number of events analyzed thus far
    count += 1;
  }

  //Write Plot to file
  h_njets->Write();
  h_mjj->Write();
  h_njets_cal->Write();
  h_mjj_cal->Write();

  TCanvas *c1 = new TCanvas("njets", "njets", 800, 600);
  h_njets->Draw();
  TCanvas *c2 = new TCanvas("mjj", "mjj", 800, 600);
  h_mjj->Draw();
  TCanvas *c3 = new TCanvas("njets_c", "njets_c", 800, 600);
  h_njets_cal->Draw();
  TCanvas *c4 = new TCanvas("mjj_c", "mjj_c", 800, 600);
  h_mjj_cal->Draw();


  c1->SaveAs("njets.pdf");
  c2->SaveAs("mjj.pdf");
  c3->SaveAs("njets_cal.pdf");
  c4->SaveAs("mjj_cal.pdf")  ;

  //Close output file
  oFile->Close();

  // exit from the main function cleanly
  return 0;
}
